import {Toast } from 'mint-ui'

export default{
   /* 加入购物车 */
   addToCart(state,item) {
       const has = state.cart.some(curr=>{
           return curr.id==item.id           
        }) 
       if(has){
          state.cart = state.cart.filter(curr=>{
              if(curr.id==item.id){curr.amount += 1}
              return true
          })
       }else{
           item.amount=1
           state.cart.push(item)
       }
       Toast({
         message: '加入成功',
         position: 'bottom',
         duration: 1000
       })
   },
   /* 立即购买 */
   buySoon(state,infor){
        infor.ischecked=true
        const has = state.cart.some(curr=>{
            return curr.id == infor.id
        })
        if(has){
           state.cart=state.cart.filter(curr=>{
               if (curr.id===infor.id) curr.amount += 1
               return true
           })
        }else{
            infor.amount=1
            state.cart.push(infor)
        }  
        window.location.reload()
   },
    /* 商品数量增加操作 */
    addNum(state,id){       
         state.cart = state.cart.filter(curr=>{
             if(curr.id===id){
                 curr.amount+=1;
             }
             return true
         })
    },
    minusNum(state,id){
       state.cart = state.cart.filter(curr=>{
           if(curr.id===id && curr.amount>=1)
              curr.amount -= 1;
              return true
       })
    },
    // 删除结算操作
    delCart(state, checkedId) {
        state.cart.filter((curr) => {
            if (curr.ischecked) {
            checkedId.push(curr.id)
            }
        })
        /* 未选中删除商品 */
        if (checkedId.length === 0) {
            Toast({
            message: '请选择要删除的商品',
            position: 'center',
            duration: 1000
            })
        }else{
            checkedId.map(curr => {
                state.cart.forEach((item,i) => {
                    if (item.id == curr) {
                        state.cart.splice(i, 1)                         
                    }
                })               
            })
            if (state.cart.length < 1) {
              window.location.reload()
            }
        }
       
    },
    // 全选操作
    checkAll(state) {
         state.isCheckedAll = !state.isCheckedAll
         state.cart.map(curr => {
            curr.ischecked =state.isCheckedAll
            return curr
         })
    },
    // 单选操作
    checkOne(state,id) {
        state.cart.map(curr=>{
            if(curr.id===id)
              curr.ischecked = !curr.ischecked
              return curr
        })
        let isAll= state.cart.some(curr => curr.ischecked==false)        
        state.isCheckedAll = !isAll
    },
    changeCheck(state){
        state.cart = state.cart.filter(curr =>{
            curr.ischecked = true
            state.isCheckedAll = true
            return true
        })
    },
    /* 编辑按钮 */
    editDel(state) {       
        state.isEdit=!state.isEdit
        state.cart = state.cart.filter(curr=>{
            if(state.isEdit) {
                curr.ischecked=false
                state.isCheckedAll = false
            } else{
                curr.ischecked = true
                state.isCheckedAll=true
            }
            return true
        })
    },
    //修改登录状态
    modifyLoginState(state, isLogin) {
        state.isLogin = isLogin
    }
}