export default{
    totalMoney(state){
        let totalmoney = 0
        state.cart.filter(curr=>{
            if(curr.ischecked){
                totalmoney += curr.amount * curr.price
            }
             return true
        })   
        return totalmoney
    },
    totalAmount(state){
       let totalamount = 0
       state.cart.filter(curr => {
         if (curr.ischecked) {
           totalamount += curr.amount
         }
         return true
       })
       return totalamount
    },
    totalAmountAll(state) {
      let totalAmountAll = 0
      state.cart.filter(curr => {
        totalAmountAll += curr.amount
        return true
      })
      return totalAmountAll
    }
}