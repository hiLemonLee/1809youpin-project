import Vue from "vue"
import Vuex from "vuex"
import mutations from "./mutations.js"
import state from "./state.js"
import getters from './getters'
Vue.use(Vuex)

const saveStorage = store=>{
   store.subscribe((mutation,state)=>{
        localStorage.setItem('cart',JSON.stringify(state.cart))
   })
}
export default new Vuex.Store({
    strict:true,
    plugins:[saveStorage],
    state,
    mutations,
    getters
})
