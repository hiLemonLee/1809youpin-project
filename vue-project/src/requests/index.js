import axios from 'axios'
import { Indicator, Toast } from 'mint-ui'

//创建一个 axios 实例
const ajax = axios.create({
    baseURL: 'http://rap2api.taobao.org/app/mock/121908'
})

//添加一个请求拦截器
ajax.interceptors.request.use(config => {
        Indicator.open({
            text: 'Loading...',
            spinnerType: 'fading-circle'
        })
        return config;
    })
    //添加一个响应拦截器
ajax.interceptors.response.use(res => {
    Indicator.close();
    if (res.data.res_code === 1) {
        return res.data.res_body
    } else {
        Toast({
            message: '提示:数据请求超时',
            position: 'center',
            duration: 1000
        })
    }
    return res
})

//"品味"页面轮播图数据
export const getMyTestsSwipeData = () => {
        return ajax.get('/api/swiperImgs')
    }
    //"品味"页面小导航数据请求
export const getMyTestNav = () => {
        return ajax.get('/api/testlist')
    }
    //"品味"页面weeklist数据请求
export const getMyTestWeekList = () => {
        return ajax.get('/api/testweekly')
    }
    //获取首页轮播图数据
export const getHomeSwiper = () => {
        return ajax.get('/api/swiperimgs')
    }
    //获取首页导航数据
export const getHomeNav = () => {
        return ajax.get('/api/homenav')
    }
    //获取首页广告图数据
export const getHomeBanner = () => {
        return ajax.get('/api/bannerimgs')
    }
    //获取首页限时购列表数据
export const getHomeLimitedTime = () => {
        return ajax.get('/api/limitedTimeList')
    }
    //获取首页新品列表数据
export const getHomeNew = () => {
        return ajax.get('/api/newList')
    }
    //请求分类栏目数据
export var getCategoryList = () => {
        return ajax.get('/api/categorylist');
    }
    //请求分类物品数据
export const getProductList = (categoryId) => {
        return ajax.get('/api/productlist?categoryId=' + categoryId);
    }
    //请求栏目货物详情数据
export const getgoodscategorylist = () => {
        return ajax.get('/api/goodscategorylist');
    }
    //请求详情页面数据
export const getDetailList = () => {
        return ajax.get('/api/DetailList');
    }
    //请求详情页面广告图数据
export const getDetailSwipe = () => {
        return ajax.get('/api/DetailSwipe');
    }
    //请求购物车“为您推荐”数据
export const getCartRecoList = () => {
    return ajax.get('/api/cartlist')
}
//请求专属推荐列表数据
export const getMineList = () => {
    return ajax.get('/api/mineList')
}
//用户登录
export const postLogin = () => {
    return ajax.post('/api/login')
}