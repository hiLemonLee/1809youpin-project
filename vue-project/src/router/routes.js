// import Home from '../pages/Home/Home.vue'
// import Category from '../pages/Category/Category.vue'
// import MyTests from '../pages/MyTests/MyTests.vue'
// import Cart from '../pages/Cart/Cart.vue'
//import Mine from '../pages/Mine/Mine.vue'

//懒加载
import Tabbar from '../components/Tabbar'
import goTop from '../components/goback'
import Search from '../components/Search'
import Detail from '../components/Detail'
const Home = () =>
    import ('@/pages/Home/Home.vue')
const Category = () =>
    import ('@/pages/Category/Category.vue')
const CategoryList = () =>
    import ('@/pages/Category/CategoryList.vue')
const Goodscategory = () =>
    import ('@/pages/Category/Goodscategory.vue')
const MyTests = () =>
    import ('@/pages/MyTests/MyTests.vue')
const Cart = () =>
    import ('@/pages/Cart/Cart.vue')
const Mine = () =>
    import ('@/pages/Mine/Mine.vue')
const DetailMyTestNav = () =>
    import ('@/pages/MyTests/DetailMyTestNav.vue')
const HomeHeader = () =>
    import ('@/pages/Home/HomeHeader.vue')
const Login = () =>
    import ('@/pages/Login/Login.vue')
const PersonalData = () => 
    import ('@/pages/PersonalData/PersonalData.vue')

const routes = [{
        path: '/',
        redirect: '/home',
        meta: {
            isNav: false
        }
    },
    {
        path: '/home',
        name: "home",
        components: {
            head: HomeHeader,
            main: Home,
            tabbar: Tabbar
        },
        meta: {
            navTitle: "首页",
            isNav: true,
            icon: 'icon-men'
        }
    },
    {
        path: '/Category',
        name: "category",
        components: {
            main: Category,
            tabbar: Tabbar
        },
        meta: {
            navTitle: "分类",
            isNav: true,
            icon: 'icon-guizi'
        },
        children: [{
            path: ":categoryId",
            name: "categorylist",
            components: {
                default: CategoryList
            }
        }]
    },
    {
        path: '/Mytests',
        name: "mytest",
        components: {
            main: MyTests,
            tabbar: Tabbar
        },
        meta: {
            navTitle: "品味",
            isNav: true,
            icon: 'icon-zhangxuyangcansaitubiao-'
        }
    },
    {
        path: '/Cart',
        name: "cart",
        components: {
            main: Cart,
            tabbar: Tabbar
        },
        meta: {
            navTitle: "购物车",
            isNav: true,
            icon: 'icon-icon-test'
        }
    },
    {
        path: '/Mine',
        name: 'mine',
        components: {
            main: Mine,
            tabbar: Tabbar
        },
        meta: {
            navTitle: "个人",
            isNav: true,
            icon: 'icon-gerensvg',
        }
    },
    {
        path: '/DetailMyTestNav/:id',
        name: "detailMyTestNav",
        components: {
            main: DetailMyTestNav,
            goTop: goTop
        },
        meta: {
            isNav: false
        }
    },
    {
        path: '/Search',
        name: 'search',
        components: {
            main: Search
        },
        meta: {
            isNav: false,
        }
    },
    {
        path: '/Detail/:id',
        name: 'detail',
        components: {
            main: Detail
        },
        meta: {
            isNav: false,
        }
    },
    {
        path: "/Goodscategory/:id",
        name: "goodscategory",
        components: {
            main: Goodscategory
        },
        meta: {
            isNav: false,
        }
    },
    {
        path: "/Login",
        name: "login",
        components: {
            main: Login
        },
        meta: {
            isNav: false
        }
    },
    {
        path: "/PersonalData",
        name: "personalData",
        components: {
            main: PersonalData
        },
        meta: {
            isNav: false
        }
    }
]
export default routes;