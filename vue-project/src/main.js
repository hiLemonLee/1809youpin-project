// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from "./store"
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
import './assets/jquery-1.11.3'
// 引入 requests 目录下的所有接口
import * as $http from './requests'
Vue.prototype.$http = $http

Vue.config.productionTip = false
Vue.use(MintUI)


/* 全局mixin */
Vue.mixin({
    filters:{
        moneyFormat(v){
            return Number(v).toFixed(2)
        }
    }
})
/* eslint-disable no-new */
new Vue({
    el: '#app',    
    store,
    router,
    components: { App },
    template: '<App/>'
})